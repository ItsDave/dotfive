# Deliverables

#### How long did you spend on the coding test?
I think it was around 4 hours in total, but it was hard to keep track as I had to keep putting it down - thanks kids!

#### Did you manage to cover everything that you wanted to?

In short no. I'm disgraced with what i'm supplying (especially the UI!). Unfortunately I had far too many distractions over the weekend to really get my head into it.

#### What would you add to your solution if you had more time?

So, i've tried to touch on various technical aspects to show a range of knowledge with the time I was able to dedicate to it.

If I was able to spend more time on it I would have liked to:

* Not use polling for updates, I hate it. I started to migrated it to use Pusher, which is also powering the browser notifcations for push events. Unfortunately, it came down to implementation time.

* TDD/ BDD - I would have liked to of written the tests before writing the application logic, however trying to stick to the timescales provided, I didn't want to just produce a repository full of tests. So made the conscious decision to not use TDD unfortunately.

* UI, I would have loved to implemented a lovely drag n' drop system with inline editing

* Potentially implementing Nested Sets to make the queries more efficient.

* Actually finish the functionality!

* Tidy up the event broadcasts

#### Would you choose different technologies if this were to become a reliable enterprise system? Why? Or, why not?
I would definitely look at using Nested Sets, and Pusher/Socket.io rather than pulling.

Also if it was going into a production environment, i'm all for RBAC and Auditing, Soft Deletes etc.

I have no concerns with Laravel/ Vue being used in a reliable enterprise system, just the implementations of them.

## Feedback

To be honest, it's probably the best technical test i've come across during my career. The questions were varied, thought provoking and interesting. While the technical test covered a wide range of technical concepts.

You should all be proud.

# Installation Guide

First, clone the repository

`git clone`

Then do a composer install `composer install` or `php phar.php composer` depending on your set up.

Then, move the default config file to set all of the environment files

`mv .env.example .env`

Open the `.env` file and enter in the MySQL/ Database credentials as you wish (set to use a local sqlite database by default)

### Migrate the Database

Run `php artisan db:migrate` which will create all of the required database tables

### Seed the Database (optional)

I've used faker to create some fake Items/ Categories to play with. If required, these can be generated with:

`php artisan db:seed`

### Spin up a webserver

The application works fine using PHP's development server, which can be created using `php artisan serve` this will make the application accessible at `http://localhost:8000`

### Registration

You will need users to complete the testing, users can be created at the login form using the registration page locaed at:
`http://localhost:8000/register`

I think that's about it!

Thank you for your time viewing it.

### Testing

There is a whopping 2 integration tests, which test 2 of the JSON endpoints.

`vendor/bin/phpunit` should run them for you.

