<?php

namespace App;

use App\Events\DeletedCategoryEvent;
use App\Events\NewCategoryEvent;
use App\Events\UpdatedCategoryEvent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var array
     */
    public $with = ['items', 'children'];

    /**
     * @var array
     */
    protected $attributes = [
        'parent_id' => 0,
    ];

    /**
     * @var array
     */
    public $fillable = [
        'name',
        'parent_id',
    ];

    /**
     * Fire events to send the broadcast to Pusher
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => NewCategoryEvent::class,
        'updated' => UpdatedCategoryEvent::class,
        'deleted' => DeletedCategoryEvent::class,
    ];

    public static function getByParentId($id = 0) : Collection
    {
        return self::whereParentId(0)->orderBy('name')->get();
    }

    /**
     * @param $data
     * @param string $prefix
     * @return array
     * @internal param Collection $collection
     * @internal param $array
     */
    public static function recurse($data, $prefix = '') {
        $separator = ($prefix == '' ? '' : ' > ');
        $return = array();

        if (is_array($data)) {
            foreach($data as $key => $value) {

                $return[] = [
                    'id' => $value['id'],
                    'name' => $prefix . $separator . $value['name'],
                ];

                $return = array_merge($return, self::recurse($value['children'],$prefix . $separator . $value['name']));
            }
        }

        return $return;
    }

    /**
    * @return mixed
    */
    public function items()
    {
        return $this->hasMany('App\Item');
    }

    /**
     * @return mixed
     */
    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function getStructure ()
    {
        $this->getChildren($this->children);
    }
}
