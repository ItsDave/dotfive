<?php

namespace App\Http\Controllers;

use App\Category;
use App\Events\NewItemEvent;
use App\Events\UpdatedItemEvent;
use App\Http\Requests\DeleteItemRequest;
use App\Http\Requests\NewCategoryRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Return a list of all of the categories
     * @return mixed
     * @internal param Request $request
     */
    public function getList() : JsonResponse
    {
        $categories = Category::getByParentId(0);

        return response()->json($categories);
    }

    /**
     * Get a structured list of all the categories for the select box.
     * I REALLY dislike this method, and wish I had picked Nested Sets from
     * the start
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll() : JsonResponse
    {
        $categories =  Category::getByParentId(0);

        $structuredList = collect(Category::recurse($categories->toArray()));

        return response()->json($structuredList);
    }

    /**
     * Save/ update the category accordingly
     * @param NewCategoryRequest $request
     */
    public function save(NewCategoryRequest $request)
    {
        if ($request->has('id')) {
            $category = Category::findOrFail($request->get('id'));
        } else {
            $category = new Category();
        }

        $category->fill($request->all(['name', 'id', 'parent_id']));
        $category->save();

        abort(201);
    }

    /**
     * Delete a category, and all of it's children
     * @param $category
     */
    public function delete($category)
    {
        $category = Category::findOrFail($category);

        // We need to make sure that we remove the children or they'd be orphans
        $category->children()->delete();
        $category->items()->delete();

        $category->delete();

        abort(202);
    }
}
