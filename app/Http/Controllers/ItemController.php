<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteItemRequest;
use App\Http\Requests\NewCategoryRequest;
use App\Http\Requests\NewItemRequest;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Save/ update the category accordingly
     * @param NewItemRequest $request
     */
    public function save(NewItemRequest $request)
    {
        if ($request->has('id')) {
            $item = Item::findOrFail($request->get('id'));
        } else {
            $item = new Item();
        }

        $item->fill($request->all(['title', 'id', 'category_id']));
        $item->save();

        abort(201);
    }

    /**
     * Delete an item
     * @param DeleteItemRequest $request
     */
    public function delete(DeleteItemRequest $request)
    {
        $item = Item::destroy($request->get('id'));

        if ($item) {
            abort('204');
        }
    }
}
