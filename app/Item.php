<?php

namespace App;

use App\Events\DeletedItemEvent;
use App\Events\NewItemEvent;
use App\Events\UpdatedItemEvent;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $attributes = [
        'category_id' => 0,
    ];

    /**
     * @var array
     */
    protected $fillable = [
      'title',
      'parent_id',
    ];

    /**
     * Fire events to send the broadcast to Pusher
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => NewItemEvent::class,
        'updated' => UpdatedItemEvent::class,
        'deleted' => DeletedItemEvent::class,
    ];

    /**
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
