<?php

use Faker\Generator as Faker;

$factory->define(\App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->domainWord(),
        'parent_id' => \App\Category::inRandomOrder()->first() ?? 0,
    ];
});
