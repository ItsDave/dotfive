<?php

use Faker\Generator as Faker;

$factory->define(\App\Item::class, function (Faker $faker) {
    return [
        'title' => $faker->domainWord,
        'category_id' => \App\Category::inRandomOrder()->first(),
    ];
});
