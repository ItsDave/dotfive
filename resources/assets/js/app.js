
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('category', require('./components/CategoryComponent.vue'));

const eventBus = new Vue();

const app = new Vue({
    el: '#app',

    data: {
        current: {
            id: 0,
            parent_id: 0,
        },

        categories: [],
        structuredList: [],
        loading: true,

        showItemEdit: false,
        showCategoryEdit: false,
        editCategoryData: {},
        editItemData: {},

        error: false,
    },

    computed: {
        sortedArray: function() {
            function compare(a, b) {
                if (a.name < b.name)
                    return -1;
                if (a.name > b.name)
                    return 1;
                return 0;
            }

            console.log(this.structuredList);

            return this.structuredList.sort(compare);
        }
    },

    methods: {

        loadData: function () {
            axios.get('/api/category').then(response => {
                this.categories = response.data;
                this.loading = false;
            });

            axios.get('/api/category/all').then(response => {
                this.structuredList = response.data;
            });
        },

        createCategory: function () {
            this.editCategory = {};
            this.showItemEdit = false;
            this.showCategoryEdit = true;
        },

        editCategory: function (category) {
            this.showItemEdit = false;
            this.showCategoryEdit = true;

            this.editCategoryData = category;
        },

        saveCategory: function (category) {
            axios.post('/api/category', category).then(response => {
                this.loadData();
                this.editCategoryData = {};
            });
        },

        deleteCategory: function (category) {
            axios.delete('/api/category/delete/' + category).then(response => {
                this.showNotification("Category has been deleted");
                this.loadData();
            });
        },

        createItem: function () {
            this.editItemData = {};
            this.showItemEdit = true;
            this.showCategoryEdit = false;
        },

        deleteItem: function (item) {
            axios.delete('/api/item/delete/' + item).then(response => {
                this.showNotification("Item has been deleted");
                this.loadData();
            });
        },

        saveItem: function (item) {
            axios.post('/api/item', item).then(response => {
                this.showNotification("Item Saved");
                this.loadData();
                this.editItemData = {};
            });
        },

        editItem: function (item) {
            this.showItemEdit = true;
            this.showCategoryEdit = false;

            this.editItemData = item;
        },

        showNotification: function(text) {
            if (Notification.permission === "granted") {
                var notification = new Notification(text);
            }
        }
    },

    mounted: function() {
        this.loadData();

        eventBus.$on('editItem', this.editItem);
        eventBus.$on('editCategory', this.editCategory);

        eventBus.$on('deleteCategory', this.deleteCategory);
        eventBus.$on('deleteItem', this.deleteItem);

        setInterval(function () {
            this.loadData();
        }.bind(this), 10000);

        Echo.channel('updates')
            .listen('UpdatedCategoryEvent', (e) => {
                this.showNotification('Updated Category: ' + e.category.name)
            })
            .listen('NewCategoryEvent', (e) => {
                this.showNotification('Created Category: ' + e.category.name)
            });
    }
});

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return eventBus
        }
    }
});

Notification.requestPermission();
