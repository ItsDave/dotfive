@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Categories</div>

                <div class="card-body">

                    <h2>@{{ _.has(current, 'name') ? current.name : 'Categories' }}</h2>

                    <button type="button" class="btn btn-primary" @click="createCategory()">New Category</button>
                    <button type="button" class="btn btn-info" @click="createItem()">New Item</button>

                    <hr />

                    <div v-if="showItemEdit">
                        <h3>New/Edit Item</h3>
                        Parent: <select v-model="editItemData.category_id" style="max-width: 100%; width:100%; margin-bottom:10px;">
                                    <option v-for="(item, index) in structuredList" :value="item.id">@{{ item.name }}</option>
                                </select>
                        <br/>
                        Name: <input type="text" value="title" v-model="editItemData.title"/> <input type="submit" @click="saveItem(editItemData)"/>
                        <hr />
                    </div>

                    <div v-if="showCategoryEdit">
                        <h3>New/Edit Category</h3>
                        Parent: <select v-model="editCategoryData.parent_id" style="max-width: 100%; width:100%;  margin-bottom:10px;">
                                    <option v-for="(category, index) in structuredList" :value="category.id">@{{ category.name }}</option>
                                </select>
                        <br/>
                        Name: <input type="text" value="name" v-model="editCategoryData.name"/> <input type="submit" @click="saveCategory(editCategoryData)"/>
                        <hr />
                    </div>

                    <div v-if="loading">Loading...</div>

                    <ul v-show="loading == false" style="display: none">

                        <li class="category" v-for="category in categories">
                            <category :current="category" v-on:delete="deleteCategory"></category>
                        </li>
                        <li v-if="categories.length == 0">Sorry, there is nothing to see here</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
