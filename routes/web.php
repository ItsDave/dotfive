<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function() {
    Route::get('/api/category', 'CategoryController@getList');

    Route::get('/api/category/all', 'CategoryController@getAll');

    Route::post('/api/category', 'CategoryController@save');
    Route::delete('/api/category/delete/{id}', 'CategoryController@delete');


    Route::post('/api/item', 'ItemController@save');
    Route::delete('/api/item/delete/{id}', 'ItemController@remove');
});