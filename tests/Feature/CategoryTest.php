<?php

namespace Tests\Feature;

use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    /**
     * Ensure we can get a valid JSON response containing the categories
     * @test
     * @return void
     */
    public function can_get_json_categories()
    {
        $this->withoutMiddleware();

        $request = $this->get('/api/category');

        $request->assertSuccessful();
        $request->assertJsonFragment(['name']);
    }

    /**
     * Ensure we can get a valid JSON response containing the categories
     * @test
     * @return void
     */
    public function can_create_category()
    {
        $this->withoutMiddleware();

        $parentId = factory(Category::class)->create();

        $request = $this->post('/api/category', [
            'name' => 'Test Category',
            'parent_id' => $parentId->id,
        ]);

        $request->assertSuccessful();
    }
}
